# Remove New Lines

## Description

So, sometimes when you find your favorite book online, you may find it all written in HTML.  If you copy and paste this HTML to a word processor, such as word or notepad, you'll find that word wrap no longer works due to inappropriate line breaks.

Well, I fixed that.

This program takes a file with unnecessary line breaks and replaces them by a string.  No, it is not smart enough to detect hyphens.  For instance, if you want to get The Odyssey from MIT's site, you'll find that it is written completely in this format.  This program will read an input file and replace each new line with the a space, skipping double spaces indicating a paragraph.  From there, you can can copy the contents from the text file to Word, or any other word processor, and have the entire book in line-wrapped form.

## Usage

Run the CMake file for compilation, or compile it how you see fit.  It's only a single "main.cpp" and doesn't require any crazy libraries.

```bash
mkdir build
cd build
cmake ../.
make
```

Define the input file, which should be a basic ASCII text file.

```bash
./RemoveNewLines -f input.txt
```

The output will by default go to output.txt.  You can specify an output file as well.

```bash
./RemoveNewLines -f input.txt -o output.txt
```


