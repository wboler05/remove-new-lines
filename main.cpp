#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <vector>

struct Params {
  std::string input_filename;
  std::string output_filename = "output.txt";

};

Params parse_args(int argc, char * argv[]) {

  Params params;

  for (int i = 0; i < argc; i++) {
    std::string argument = argv[i];

    if (argument == "-f") {
      if (i+1 == argc) {
        std::cerr << "Error, -f requires <file> argument." << std::endl;
        exit(1);
      } else {
        i++;
        params.input_filename = argv[i];
      }
    } else if (argument == "-o") {
      if (i+1 == argc) {
        std::cerr << "Error, -o requires <file> argument." << std::endl;
        exit(1);
      } else {
        i++;
        params.output_filename = argv[i];
      }
    }
  }

  std::cout << "Input File: " << params.input_filename << std::endl;
  std::cout << "Output File: " << params.output_filename << std::endl;

  return params;

}

std::string clean_seperator(Params params) {

  std::ifstream input;
  input.open(params.input_filename);
  std::string s;

  while (!input.eof()) {

    char c[2];
    c[1] = 0;
    input.get(c[0]);

    if (c[0] == '\r') {
      continue;
    } else {
      s.append(c);
    }
  }

  return s;

  std::cerr << "Error, cannot figure out the file line termination." << std::endl;
  exit(1);

}

std::vector<std::string> split(const std::string & s) {

  std::vector<std::string> oss;
  size_t prev;

  prev = 0;
  for (size_t i = 0; i < s.size(); i++) {
    if (s[i] == '\n') {
      std::cout << "Match( i = " << i << ", j = " << prev << "): ";
      std::string sub = s.substr(prev, i - prev);
      std::cout << sub << std::endl;
      oss.push_back(sub);
      prev = i+1;
    }
  }

  std::cout << "Line list: " << std::endl;
  for (size_t i = 0; i < oss.size(); i++) {
    std::cout << " - " << oss[i] << std::endl;
  }

  return oss;
}

void output_correction(Params params, std::string cleanInput) {

  std::ofstream outputFile;

  std::string oss;

  std::vector<std::string> split_string = split(cleanInput);

  outputFile.open(params.output_filename, std::ios_base::trunc);
  for (size_t i = 0; i < split_string.size(); i++) {
    if (split_string[i].size() == 0) {
      oss += "\n\n";
    }

    oss += split_string[i] + " ";
  }

  std::cout << oss << std::endl;

  outputFile << oss;

  outputFile.close();
}

void test_file(Params params) {

  std::ifstream inputFile;
  inputFile.open(params.input_filename, std::ios_base::in);
  if (inputFile.is_open()) {
    inputFile.close();
  } else {
    std::cerr << "Unable to open file." << std::endl;
    exit(1);
  }
}

int main(int argc, char * argv[]) {

  Params params = parse_args(argc, argv);

  test_file(params);

  std::string cleanInput = clean_seperator(params);

  output_correction(params, cleanInput);

  return 0;

}
